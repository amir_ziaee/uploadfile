<?php

class file
{
    private $_suportedFormats = ['image/png','image/jpeg','image/jpg','image/gif'];
    private $src = "C:/xampp/htdocs/ui/uploads/";
    public function uploadFile($file){
        if (is_array($file)){
            if (in_array($file['type'],$this->_suportedFormats)){
                move_uploaded_file($file['tmp_name'],$this->src.$file['name']);
            }else{
                die("format not suported");
            }

        }else{
            die("no file uploaded");
        }
    }

}
